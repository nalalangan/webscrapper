﻿Imports System.IO
Imports System.Net
Imports System.Net.Http

Public Class WebScrapper
  Public Async Function StartScrapping() As Task
    ScrapedStrings = Nothing
    Dim counter = 0
    Dim top10 = 0
    Dim tempScrapedStrings = (Await (GetPageAsync("http://www.investing.com/equities/top-stock-gainers"))).Split(vbLf)

    For Each scrapedlines As String In tempScrapedStrings

      If (scrapedlines.Contains("title='")) Then
        scrapedlines = scrapedlines.Remove(0, scrapedlines.IndexOf("'") + 1)
        scrapedlines = scrapedlines.Remove(scrapedlines.IndexOf("'"), scrapedlines.Length - scrapedlines.IndexOf("'"))
        'AddtoArray(ScrapedStrings, Nothing)

        ReDim Preserve ScrapedStrings(top10)
        AddtoArray(ScrapedStrings(top10), scrapedlines)
        counter = 6
      ElseIf (counter > 0) Then
        Select Case counter
          Case 6 'STOCK CLOSE PRICE
            'Don't need

          Case 5 'STOCK HIGHEST
            'Don't need

          Case 4 'STOCK LOWEST
            'Don't need

          Case 3 'CHANGE FROM MARKET OPEN IN DOLLARS
            'Don't need

          Case 2 'CHANGE FROM MARKET OPEN IN PERCENTAGE
            scrapedlines = scrapedlines.Remove(0, scrapedlines.IndexOf(">") + 1)
            scrapedlines = scrapedlines.Remove(scrapedlines.IndexOf("<"), scrapedlines.Length - scrapedlines.IndexOf("<"))

            AddtoArray(ScrapedStrings(top10), scrapedlines)

          Case 1 'STOCK VOLUME
            scrapedlines = scrapedlines.Remove(0, scrapedlines.IndexOf(">") + 1)
            scrapedlines = scrapedlines.Remove(scrapedlines.IndexOf("<"), scrapedlines.Length - scrapedlines.IndexOf("<"))

            AddtoArray(ScrapedStrings(top10), scrapedlines)
            top10 += 1
        End Select

        counter -= 1
      End If
      If top10 = 10 Then Exit For
    Next

  End Function

  Private Sub AddtoArray(ByRef Obj As Object(), item As Object)
    Dim arraycount = 0
    If Not (IsNothing(Obj)) Then arraycount = Obj.Count

    ReDim Preserve Obj(arraycount)
    Obj(arraycount) = item
  End Sub

  Private Async Function GetPageAsync(ByVal URL As String) As Task(Of String)
    Dim client As New HttpClient
    Dim stroutput As String = ""
    client.DefaultRequestHeaders.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)")
    Try
      stroutput = Await (client.GetStringAsync(New Uri(URL)))
    Catch ex As Exception
      MsgBox(ex.Message.ToString)
    End Try
    Return stroutput
  End Function
End Class
