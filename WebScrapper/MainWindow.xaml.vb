﻿Imports OxyPlot
Imports OxyPlot.Series

Class MainWindow

  Private WithEvents Scrapper As New WebScrapper

  '================================================DATA TO BE PARSED================================================'
  '0          Wins Finance Holdings Inc                                   STOCK NAME
  '6  		    <td Class="align_right pid-942500-last" >16.98</td>         STOCK CLOSE PRICE
  '5          <td Class="align_right pid-942500-high" >17.00</td>         STOCK HIGHEST
  '4          <td Class="pid-942500-low" >9.34</td>                       STOCK LOWEST
  '3          <td Class="bold greenFont pid-942500-pc" >+8.68</td>        CHANGE FROM MARKET OPEN IN DOLLARS
  '2          <td Class="bold greenFont pid-942500-pcp" >+104.64%</td>    CHANGE FROM MARKET OPEN IN PERCENTAGE
  '1          <td Class="pid-942500-turnover" >36.11K</td>                STOCK VOLUME
  '================================================================================================================'

  Private Sub ButtonScrap_Click(sender As Object, e As RoutedEventArgs) Handles ButtonScrap.Click
    Dim FS As New LineSeries

    ButtonScrap.Content = 0
    FS.Title = "Data1"
    FS.MarkerType = MarkerType.Diamond
    FS.Points.Add(New DataPoint(0, 0))
    FS.Points.Add(New DataPoint(10, 10))

    Dispatcher.Invoke(Sub()


                        PlotViewGain.Model.Series.Add(FS)

                      End Sub)
    'While (1)

    '  Await Scrapper.StartScrapping()
    '  ButtonScrap.Content = ButtonScrap.Content + 1

    '  If IsNothing(ScrapedStrings) Then Exit Sub
    '  TextBoxData.Text = ScrapedStrings(0)(0) & vbTab & vbTab & ScrapedStrings(0)(1) & vbTab & vbTab & ScrapedStrings(0)(2) & vbLf &
    '      ScrapedStrings(1)(0) & vbTab & vbTab & ScrapedStrings(1)(1) & vbTab & vbTab & ScrapedStrings(1)(2) & vbLf &
    '      ScrapedStrings(2)(0) & vbTab & vbTab & ScrapedStrings(2)(1) & vbTab & vbTab & ScrapedStrings(2)(2) & vbLf &
    '      ScrapedStrings(3)(0) & vbTab & vbTab & ScrapedStrings(3)(1) & vbTab & vbTab & ScrapedStrings(3)(2) & vbLf &
    '      ScrapedStrings(4)(0) & vbTab & vbTab & ScrapedStrings(4)(1) & vbTab & vbTab & ScrapedStrings(4)(2) & vbLf &
    '      ScrapedStrings(5)(0) & vbTab & vbTab & ScrapedStrings(5)(1) & vbTab & vbTab & ScrapedStrings(5)(2) & vbLf &
    '      ScrapedStrings(6)(0) & vbTab & vbTab & ScrapedStrings(6)(1) & vbTab & vbTab & ScrapedStrings(6)(2) & vbLf &
    '      ScrapedStrings(7)(0) & vbTab & vbTab & ScrapedStrings(7)(1) & vbTab & vbTab & ScrapedStrings(7)(2) & vbLf &
    '      ScrapedStrings(8)(0) & vbTab & vbTab & ScrapedStrings(8)(1) & vbTab & vbTab & ScrapedStrings(8)(2) & vbLf &
    '      ScrapedStrings(9)(0) & vbTab & vbTab & ScrapedStrings(9)(1) & vbTab & vbTab & ScrapedStrings(9)(2)

    '  'If IsNothing(FS.Points) Then
    '  '  FS.Points.Add(New DataPoint(0, 0))
    '  'Else
    '  '  FS.Points.Add(New DataPoint(FS.Points.Count, 0))
    '  'End If

    'End While
    'For Each StringsArray In ScrapedStrings
    '  For Each Strings In StringsArray
    '    TextBoxData.AppendText(Strings.ToString & vbTab & vbTab)
    '  Next Strings
    '  TextBoxData.AppendText(vbLf)
    'Next StringsArray
  End Sub
End Class

Module Data
  Public ScrapedStrings()()
End Module
